package tdd.training.bsk;

import static org.junit.Assert.*;
import org.junit.Test;

public class GameTest {

	//Test user story 3
	@Test
	public void testAddFrameAndGetFrameAt() throws BowlingException {
		//this method tests the class Game method addFrame() and getFrameAt()
		Frame f = new Frame(3, 4);
		Game g = new Game();
		g.addFrame(f);
		assertEquals(f, g.getFrameAt(0));
	}
	
	@Test(expected = BowlingException.class)
	public void testGame() throws BowlingException{
		//this method tests the number of frames in a game (maximum 10) *expected a BowlingException*
		Game g = new Game();
		int i;
		for(i=0;i<13;i++) {
			g.addFrame(new Frame(1,5));
		}
		assertEquals(g.games.size(), 10);
	}
	
	//Test user story 4
	@Test
	public void testCalculateScore() throws BowlingException {
		//this method tests the class Frame method calculateScore() 
		Game g = new Game();
		int i;
		for(i=0;i<10;i++) {
			g.addFrame(new Frame(1,5));
		}
		assertEquals(g.calculateScore(), 60);	
	}
	
	//Test user story 5-6
		@Test
		public void testCalculateScore2() throws BowlingException {
			//this method tests the class Frame method calculateScore() 
			//when a spear and a strike occur not sequentially
			Game g = new Game();
			g.addFrame(new Frame(5,5)); 
			g.addFrame(new Frame(3,6));
			g.addFrame(new Frame(7,2));
			g.addFrame(new Frame(3,6));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(5,3));
			g.addFrame(new Frame(3,3));
			g.addFrame(new Frame(4,5));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(2,6));
			assertEquals(g.calculateScore(), 98);
		}
	
	//Test user story 7
		@Test
		public void testCalculateScore4() throws BowlingException {
			//this method tests the class Frame method calculateScore() 
			//when a strike and a spear occur sequentially
			Game g = new Game();
			g.addFrame(new Frame(10, 0));
			g.addFrame(new Frame(4,6));
			g.addFrame(new Frame(7,2));
			g.addFrame(new Frame(3,6));
			g.addFrame(new Frame(4,4));
			g.addFrame(new Frame(5,3));
			g.addFrame(new Frame(3,3));
			g.addFrame(new Frame(4,5));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(2,6));
			assertEquals(g.calculateScore(), 103);
		}
		
	//Test user story 8 - 9
	@Test
	public void testCalculateScoreNEW() throws BowlingException {
		//this method tests the class Frame method calculateScore() 
		//when multiple strikes and multiple spear occur
		Game g = new Game();
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10,0));
		g.addFrame(new Frame(7,2));
		g.addFrame(new Frame(3,6));
		g.addFrame(new Frame(8,2));
		g.addFrame(new Frame(5,5));
		g.addFrame(new Frame(3,3));
		g.addFrame(new Frame(4,5));
		g.addFrame(new Frame(8,1));
		g.addFrame(new Frame(2,6));
		assertEquals(g.calculateScore(), 124);
	}

	//Test user story 10
	@Test
	public void testCalculateScore7() throws BowlingException {
		//this method tests the class Frame method calculateScore() 
		//when there is a spear as the last frame
			Game g = new Game();
			g.addFrame(new Frame(1,5));
			g.addFrame(new Frame(3,6));
			g.addFrame(new Frame(7,2));
			g.addFrame(new Frame(3,6));
			g.addFrame(new Frame(4,4));
			g.addFrame(new Frame(5,3));
			g.addFrame(new Frame(3,3));
			g.addFrame(new Frame(4,5));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(2,8));
			g.setFirstBonusThrow(7);
			assertEquals(g.calculateScore(), 90);
		}

	//Test user story 11
	@Test
	public void testCalculateScore8() throws BowlingException {
			//this method tests the class Frame method calculateScore() 
			//when there is a strike as the last frame
			Game g = new Game();
			g.addFrame(new Frame(1,5));
			g.addFrame(new Frame(3,6));
			g.addFrame(new Frame(7,2));
			g.addFrame(new Frame(3,6));
			g.addFrame(new Frame(4,4));
			g.addFrame(new Frame(5,3));
			g.addFrame(new Frame(3,3));
			g.addFrame(new Frame(4,5));
			g.addFrame(new Frame(8,1));
			g.addFrame(new Frame(10,0));
			g.setFirstBonusThrow(7);
			g.setSecondBonusThrow(2);
			assertEquals(g.calculateScore(), 92);
		}

	//Test user story 12
	@Test
	public void testCalculateScore9() throws BowlingException {
			//this method tests the class Frame method calculateScore()
			//when there is a best score
			Game g = new Game();
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.addFrame(new Frame(10,0));
			g.setFirstBonusThrow(10);
			g.setSecondBonusThrow(10);
			assertEquals(g.calculateScore(), 300);
		}
}