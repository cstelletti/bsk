package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	//Tests user story 1
	@Test
	public void testGetThrows() throws BowlingException{
		//this method tests the class Frame methods: getFirstThrow() and getSecondThrow()
		Frame f = new Frame(3,4);
		assertTrue(f.getFirstThrow()==(3) && f.getSecondThrow()==4);
	}
	
	//Test user story 2
	@Test
	public void testGetScore() throws BowlingException {
		//this method tests the class Frame method getScore() 
		Frame f = new Frame(3,4);
		int score = f.getScore();
		assertEquals(score, 7);
	}
}
