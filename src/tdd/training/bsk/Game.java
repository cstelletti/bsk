package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	ArrayList<Frame> games;
	int firstBonusThrow;
	int secondBonusThrow;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.games = new ArrayList<>();
		this.firstBonusThrow=0;
		this.secondBonusThrow=0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException{
		if(games.size()==10)
			throw new BowlingException("Attention! You can insert only 10 element!");
		else
			this.games.add(frame);		
	}
	
	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		
		return games.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) {
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) {
		this.secondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() {
		int score=0;
		int i=0;
		for(i=0; i<10;i++)  {
			Frame f = getFrameAt(i);
			if(f.isSpare() && i+1<10) 
				f.setBonus(getFrameAt(i+1).getFirstThrow());
				else if(f.isStrike() && i+1<10){ 
						if(getFrameAt(i+1).isStrike() && i+2<10) 
							f.setBonus(getFrameAt(i+1).getScore()+getFrameAt(i+2).getFirstThrow());
						else if(i==8) 
							f.setBonus(getFrameAt(i+1).getScore()+firstBonusThrow);
							else
								f.setBonus(getFrameAt(i+1).getScore());
					}
			score = score + f.getScore();
			if(i==9 && f.isSpare())
				score = score+firstBonusThrow;
			else if(i==9 && f.isStrike()) 
				score = score+firstBonusThrow+secondBonusThrow;
		}
		return score;	
	}

}
