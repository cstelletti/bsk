package tdd.training.bsk;

public class Frame {
	
	int firstT;
	int secondT;
	int bonus;
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if(firstThrow+secondThrow>10)
			throw new BowlingException("Attention! The number of pins in the frame is wrong (maximum 10 pins)");
		else{
			this.firstT = firstThrow;
			this.secondT = secondThrow;
			this.bonus = 0;
		}
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		
		return firstT;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		
		return secondT;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus=bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		int sum;
		sum = firstT + secondT;
		if(this.isSpare() || this.isStrike()){
			sum = sum + bonus;
		}
		return sum;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		boolean flag=false;
		if(this.firstT==10)
			flag = true;
				
		return flag;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		int sum;
		sum = firstT + secondT;
		boolean flag=false;
		if(firstT!=10 && sum==10)
			flag=true;

		return flag;
	}
}
